import os
import redis
import numpy as np


class BaseConfig:
    # Directories' listings
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    UPLOAD_DIR = os.path.join(ROOT_DIR, "uploaded_images/")

    # Redis Config
    REDIS_HOST = "localhost"
    REDIS_PORT = 6379
    REDIS_DB = 0
    REDIS_SRVR = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    IMAGE_QUEUE = "image_queue"
    BATCH_SIZE = 32

    # Api Config
    ALLOWED_EXT = set(["png", "jpg", "bmp", "jpeg"])
    SAVE_IMAGE = True

    # Image manipulation config
    IMAGE_DTYPE = np.uint8

    # Model Config
    DNN_MODEL_DIR = os.path.join(ROOT_DIR, "model_parameters")
    DNN_MODELS = {
        "FRCNN": {
            "parameters": {
                "name": "faster_rcnn_inception_v2_coco_2018_01_28",
                "cpkt": os.path.join(DNN_MODEL_DIR, "FRCNN/frozen_inference_graph.pb"),
                "labels": os.path.join(DNN_MODEL_DIR, "FRCNN/mscoco_label_map.pbtxt"),
                "num_class": 90,
            }
        }
    }
