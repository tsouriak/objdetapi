import tensorflow as tf

from object_detection.utils import visualization_utils as vis_utils
from object_detection.utils import label_map_util


class LoadModel:

    def __init__(self, model_params=None, detection_graph=None):
        self.model_params = model_params
        self.detection_graph = detection_graph

    def _load_model_parameters(self):
        # Creates the Category Index
        label_map = label_map_util.load_labelmap(self.model_params['labels'])
        model_categories = label_map_util.convert_label_map_to_categories(
            label_map,
            max_num_classes=self.model_params['num_class'],
            use_display_name=True)
        category_index = label_map_util.create_category_index(
            model_categories)
        return category_index

    def _get_tf_session(self):
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.model_params['cpkt'], 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            sess = tf.Session(graph=self.detection_graph)
        return sess

    def _detection_graph_parameters(self):
        image_tensor = self.detection_graph.get_tensor_by_name(
            'image_tensor:0')
        # Each box represents a part of the image where a particular
        # object was detected.
        boxes = self.detection_graph.get_tensor_by_name(
            'detection_boxes:0')
        # Each score represent the level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = self.detection_graph.get_tensor_by_name(
            'detection_scores:0')
        classes = self.detection_graph.get_tensor_by_name(
            'detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name(
            'num_detections:0')
        return (image_tensor, boxes, scores, classes, num_detections)
                
