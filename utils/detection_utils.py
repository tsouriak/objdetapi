import numpy as np
from utils.model_loading_utils import LoadModel

from object_detection.utils import visualization_utils as vis_utils


class DetectionUtils:

    @staticmethod
    def np_image_expanded(image_np):
        return np.expand_dims(image_np, axis=0)

    @staticmethod
    def squeeze_detections(boxes, scores, classes):
        sBoxes = np.squeeze(boxes)
        sScores = np.squeeze(scores)
        sClasses = np.squeeze(classes).astype(np.int32)
        return (sBoxes, sScores, sClasses)

    @staticmethod
    def cluster_detections(sBoxes, sScores, sClasses, index, detections):
        for i in range(sBoxes.shape[1]):
            if sScores is None or sScores[i] > 0.5:
                box = tuple(sBoxes[i].tolist())
                if sClasses[i] in index.keys():
                    class_name = index[sClasses[i]]['name']
                detections.append((box, class_name, sScores[i]))
        return detections

    @staticmethod
    def write_on_image(image_np, sBoxes, sClasses, sScores, category_index):
        vis_utils.visualize_boxes_and_labels_on_image_array(
            image_np, sBoxes, sClasses, sScores, category_index,
            use_normalized_coordinates=True,
            line_thickness=2)

    @staticmethod
    def detect_objects(image_np, sess, detection_graph, category_index):
        # Expand image dimensions as inference models expects image to have
        # a shape of [1, None, None, 3]
        image_np_expanded = DetectionUtils.np_image_expanded(image_np)
        # Initiate detection graph
        (image_tensor, boxes, scores, classes, num_detections) = (
            LoadModel(
                detection_graph=detection_graph)._detection_graph_parameters())
        (boxes, scores, classes, num_detections) = sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})
        (sBoxes, sScores, sClasses) = DetectionUtils.squeeze_detections(
            boxes, scores, classes)
        detections = []
        detections = DetectionUtils.cluster_detections(
            sBoxes, sScores, sClasses, category_index, detections)
        DetectionUtils.write_on_image(
            image_np, sBoxes, sClasses, sScores, category_index)


        return image_np, detections

