import numpy as np
import base64
import json

from base_config import BaseConfig as cfg


class EncodeImage:

    # Encodes an image and prepares for it for storage in redis

    def __init__(self, image):
        self.image = image

    def read_encode_return(self):
        '''
            Stores image as a np array, transforms to C-contiguous form
            so it can be later restored without losing rows/columns
            correct order. Base64 encoding is required for storing
            in redis server.
        '''
        im = self.image
        im = np.asarray(
            bytearray(im),
            dtype=cfg.IMAGE_DTYPE)
        im = np.ascontiguousarray(im, dtype=cfg.IMAGE_DTYPE)
        im = base64.b64encode(im).decode('utf-8')
        return im


class DecodeImage:
    '''
        Decoding methods and image manipulations.
    '''

    def __init__(self, queue):
        self.queue = queue
        self.image = None
        self.uuid = None

    def load_json_and_decode(self):
        return json.loads(self.queue.decode('utf-8'))

    def b64_decode_im(self, image):
        image = bytes(image, encoding='utf-8')
        image = base64.decodebytes(image)
        image = np.frombuffer(image, dtype=cfg.IMAGE_DTYPE)
        return image

    def decode_payload(self):
        payload = self.load_json_and_decode()
        image = self.b64_decode_im(
            payload['image'])
        uuid = payload['id']
        return image, uuid
