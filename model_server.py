import json
import os
import tensorflow as tf
import signal
import sys
import numpy as np

from base_config import BaseConfig as cfg
from utils.detection_utils import DetectionUtils as DetUt
from utils.image_encoding import DecodeImage
from utils.model_loading_utils import LoadModel

# tensorflow object detection utils #
from object_detection.utils import visualization_utils as vis_util

from io import BytesIO
from PIL import Image


os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # Disables tf annoying debug info


class ModelServer:
    def __init__(self, model_name):
        self.cfg = cfg
        self.model = model_name
        self.model_pars = self.cfg.DNN_MODELS[self.model]["parameters"]
        self.category_index = None
        self.db = self.cfg.REDIS_SRVR

    def _exit_with_grace(self, signal, frame):
        print("Model Server is Terminating...")
        sys.exit(0)

    def read_queue(self):
        # Return a queue of images from redis
        queue = self.db.lrange(self.cfg.IMAGE_QUEUE, 0, self.cfg.BATCH_SIZE - 1)
        return queue

    def start_polling_model(self):
        # Load the frozen model in memory
        detection_graph = tf.Graph()
        sess = LoadModel(self.model_pars, detection_graph)._get_tf_session()

        # Exit with Grace
        signal.signal(signal.SIGINT, self._exit_with_grace)

        # Start polling the Redis Server for images
        while True:
            queue = self.read_queue()
            imageIDs = []
            for q in queue:
                image, imageUUID = DecodeImage(q).decode_payload()
                imageIDs.append(imageUUID)
                image = Image.open(BytesIO(image))
                image = np.array(image)
                processed_img, results = DetUt.detect_objects(
                    image, sess, detection_graph, self.category_index
                )
                image = Image.fromarray(processed_img)
                # WiP ###
                image.save(
                    os.path.join(cfg.ROOT_DIR, "processed_images/myphoto.jpg"), "JPEG"
                )
                ###
                output = []
                if results is not None:
                    for result in results:
                        response = {"BOX_COORDS": result[0], "CLASS": result[1]}

                        output.append(response)

                    self.db.set(imageUUID, json.dumps(output))
                self.db.ltrim(self.cfg.IMAGE_QUEUE, len(imageIDs), -1)

    def start_service(self):

        self.category_index = LoadModel(self.model_pars)._load_model_parameters()
        self.start_polling_model()


if __name__ == "__main__":
    Start_model = ModelServer(model_name="FRCNN")
    Start_model.start_service()
