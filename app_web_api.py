import flask
import os
import uuid
import json

from base_config import BaseConfig as cfg
from utils.image_encoding import EncodeImage


app = flask.Flask(__name__)
db = cfg.REDIS_SRVR

app.config["UPLOAD_FOLDER"] = cfg.UPLOAD_DIR


def allowed_filename(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in cfg.ALLOWED_EXT


@app.route("/")
def homepage():
    return "Do I even need a homepage?"


@app.route("/predict_endpoint", methods=["POST"])
def post_predict():
    data = {"success": False}
    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            image = flask.request.files["image"].read()
            encoded_image = EncodeImage(image).read_encode_return()
            key = str(uuid.uuid4())
            payload = {"id": key, "image": encoded_image}
            db.rpush(cfg.IMAGE_QUEUE, json.dumps(payload))
            while True:
                output = db.get(key)
                if output is not None:
                    output = output.decode("utf-8")
                    data["predictions"] = output
                    db.delete(key)
                    break
                data["success"] = True
        return flask.jsonify(data)


if __name__ == "__main__":
    print("Flask is starting...")
    app.run()
